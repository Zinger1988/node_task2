const bcrypt = require('bcryptjs');
const User = require('../models/userModel');
const Notes = require('../models/notesModel');

const getUserInfo = (req, res) => {
  const { userId } = req.user;

  User.findById(userId)
    .then(({ _id, username, createdAt: createdDate }) => {
      res.json({ user: { _id, username, createdDate } });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
};

const deleteUser = (req, res) => {
  const { userId } = req.user;

  User.findByIdAndDelete(userId)
    .then(() => Notes.deleteMany({ userId }))
    .then(() => {
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
};

const changeUserPassword = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findById(userId);
  const { oldPassword, newPassword } = req.body;

  if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
    User.findByIdAndUpdate(userId, { $set: { password: await bcrypt.hash(newPassword, 10) } })
      .then(() => {
        res.status(200).json({ message: 'Success' });
      })
      .catch((err) => {
        res.status(400).json({ message: err.message });
      });
  } else {
    res.status(400).json({ message: 'Unable to change password' });
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
