const Notes = require('../models/notesModel');

const getAllNotes = (req, res) => {
  Notes.find({ userId: req.user.userId })
    .then((result) => {
      res.status(200).json({ notes: result });
    })
    .catch((err) => {
      res.status(400).json({ message: `Bad request: ${err.message}` });
    });
};

const getNote = (req, res) => {
  const { id } = req.params;

  Notes.findOne({ _id: id, userId: req.user.userId })
    .then((result) => {
      res.status(200).json({ note: result });
    })
    .catch((err) => {
      res.status(400).json({ message: `Bad request: ${err.message}` });
    });
};

const createNote = (req, res) => {
  const { text, completed } = req.body;

  const note = new Notes({
    text,
    userId: req.user.userId,
    completed: completed || false,
  });

  note.save()
    .then(() => {
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(400).json({ message: `Bad request: ${err.message}` });
    });
};

const updateNote = (req, res) => {
  const { id } = req.params;
  const { text } = req.body;

  Notes.findOneAndUpdate({ _id: id, userId: req.user.userId }, { $set: { text } })
    .then((result) => {
      if (!result) {
        throw Error('Couldn\'t find requested note');
      }
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(400).json({ message: `Bad request: ${err.message}` });
    });
};

const markNoteAsCompleted = (req, res) => {
  const { id } = req.params;

  Notes.findOne({ _id: id, userId: req.user.userId })
    .then((result) => {
      if (!result) {
        throw Error('Couldn\'t find requested note');
      }
      const { completed } = result;
      return Notes.findByIdAndUpdate(id, { $set: { completed: !completed } });
    })
    .then(() => {
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(400).json({ message: `Bad request: ${err.message}` });
    });
};

const deleteNote = (req, res) => {
  const { id } = req.params;

  Notes.findOneAndDelete({ _id: id, userId: req.user.userId })
    .then((result) => {
      if (!result) {
        throw Error('Couldn\'t find requested note');
      }
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(400).json({ message: `Bad request: ${err.message}` });
    });
};

module.exports = {
  getAllNotes,
  getNote,
  createNote,
  updateNote,
  markNoteAsCompleted,
  deleteNote,
};
