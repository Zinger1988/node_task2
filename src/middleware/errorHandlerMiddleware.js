module.exports = function errorHandler(err, req, res) {
  res.status(500).send({ message: `Server error: ${err.message}` });
};
