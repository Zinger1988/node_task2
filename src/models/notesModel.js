const mongoose = require('mongoose');

const { Schema } = mongoose;
const notesSchema = new Schema({
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    required: false,
  },
});

const Notes = mongoose.model('Notes', notesSchema);

module.exports = Notes;
